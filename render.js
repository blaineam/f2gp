const { ipcRenderer } = require("electron");

function openPage(page) {
    ipcRenderer.send("changeWindow", page);
}

document.ondragover = document.ondrop = (ev) => {
    ev.preventDefault()
  }
  
  document.body.ondrop = (ev) => {
    ipcRenderer.send("setAlbumsFolder", ev.dataTransfer.files[0].path);
    ev.preventDefault()
  }
  let state;
  ipcRenderer.on('receiveState', (event, arg) => {
    state = arg;
    if (state.errorMsg) {
        $('.errorMsg').text(state.errorMsg);
        $('.errorMsg').show();
    }else{
        $('.errorMsg').hide();
    }
    if(state.currentPage == 'uploading' ) {
        updateProgressBar(state.progress);
    }
    coms();
  })
  function coms() {
      setTimeout(function() {
        ipcRenderer.send('getState', true);
      }, 1000);
  }
  coms();
  function updateProgressBar(progress) {
      try{
        $('.albumsProgress').width((progress.percentCompleted*100)+'%');
        $('.photosProgress').width((progress.metaData.curPhotos/progress.metaData.totPhotos*100)+'%');
        $('.albumsProgress').text((progress.curAlbum)+'/'+(progress.totAlbums)+' albums');
        $('.photosProgress').text((progress.metaData.curPhotos)+'/'+(progress.metaData.totPhotos)+' photos');
      }catch (err) {

      }
  }